const performance = require('performance-now');

const fizzbuzz = () => {
    for (let i = 1; i < 101; i++) {
        let y = '';
        if (i % 3 === 0) {y += 'Fizz'}
        if (i % 5 === 0) {y += 'Buzz'}
        console.log(y === '' ? i : y);
    }
};

const fizzbuzz2 = () => {
    for(i=1;i<101;i++){let x='';if(i%3==0){x+='fizz'}if(i%5==0){x+='buzz'}console.log(x==''?i:x)}
};

let perf = [];
for (let i = 0; i < 1; i++) {
    const t0 = performance();

    fizzbuzz();

    const t1 = performance();
    perf.push(t1 - t0);
}

let sum = 0;
perf.forEach((i) => {
    sum += i;
});

const average = sum / 100;

console.log(`Average time taken by execution: ${average}`);